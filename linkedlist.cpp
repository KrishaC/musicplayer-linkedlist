#include <iostream>
#include <conio.h>
#include <stdlib.h>
using namespace std;

class Node {
public:
		string	data;
		Node*	next;
};
class List{
	public:
			List(void)	{head = NULL;}
			~List(void);
			bool isEmpty(){ return head == NULL;	}
			Node* InsertNode(int index,string Name);
			int xNode(string Name);
			int DelNode(int index);
			string ViewMusic(int index);
			void DisplayPlaylist(void);
			int MaxCap(void);
	private:
			Node* head;
			friend class Stack;
};
class Stack : public List {
	public:
		Stack(){}
		~Stack(){}
		string Top(){
			if (head == NULL){
				cout<<" Not accepted"<<endl;
				cout<<"Stack is Empty"<<endl;
			}
			else{
				return (head->data);
				
			}
		}
		void Push(string x) { InsertNode(0,x);}
		string Pop(){
			if (head == NULL){
				cout<<"		  E R R O R"<<endl;
				cout<<"		Stack is Empty"<<endl;
		}
		else {
			int val =0;
			DelNode(val);	
			}
		}
		void DisplayStack() {DisplayPlaylist();}
};
int List::xNode(string Name){
	Node* VNode	=	head;
	int	VIndex	=	1;
	while (VNode && VNode->data != Name){
		VNode	=	VNode->next;
		VIndex++;
	}
	if (VNode) return VIndex;
	return 0;
}
int List::MaxCap(){
	int num = 0;
	Node* VNode	=	head;
	while (VNode != NULL){
		VNode	=	VNode->next;
		num++;
	}
	return (num);
} 
int List:: DelNode(int index){
	Node* pNode	=	NULL;
	Node* VNode	=	head;
	int VIndex	=	0;
	while (VIndex != index){
		pNode	=	VNode;
		VNode	=	VNode->next;
		VIndex++;
	}
	if (VNode){
		if(pNode){
			pNode->next	=	VNode->next;
			delete VNode;
		}
		else{
			head	=	VNode->next;
			delete	VNode;
		}
		return	VIndex;
	}
	return 0;

}
void List::DisplayPlaylist(){
	int num = 1;
	Node* VNode	=	head;
	while (VNode != NULL){
		cout<<num<<VNode->data<<endl;
		VNode	=	VNode->next;
		num++;
	}
}
Node* List::InsertNode(int index,string Name){
	if (index < 0) return NULL;
	int VIndex = 1;
	Node* VNode =head;
	while (VNode && index > VIndex){
		VNode	=	VNode->next;
		VIndex++;
	}
	Node* newNode =	new Node;
	newNode->data = 	Name;
	if (index==0){
			newNode->next	=	head;
			head 			=	newNode;
	}
	else{
		newNode->next	=	VNode->next;
		VNode->next	=	newNode;
	}
	return newNode;
}
List::~List(void){
	Node* VNode= head,	*nextNode	=	NULL;
	while (VNode != NULL){
		nextNode	=	VNode->next;
		delete	VNode;
		VNode	=	nextNode;
	}
}
string List::ViewMusic(int index){
	Node* VNode	=	head;
	int	VIndex	=	1;
	while (VIndex != index){
		VNode	=	VNode->next;
		VIndex++;
	}
	if (VNode) return VNode->data;
	return NULL;
}
int main(){
	string mus,num;
	List Music;
	Music.InsertNode(0," lowkey - Niki");
	Music.InsertNode(1," sanctuary - Joji");
	Music.InsertNode(2," best friend -Rex orange county ");
	Music.InsertNode(3," these nights - Rich Brian");
	Music.InsertNode(4," mirror - super junior");
	Music.InsertNode(5," feel special - twice");	
	again:
	cout<<"\nWelcome to Krisha's music player\n\n"
	      "Disclaimer: Choose 1-5 only!!\n\n"
	      "1. Add a new Song\n"
	      "2. Display Music title\n"
	      "3. Edit a Song Title\n"
	      "4. Delete a Music title\n"
	      "5. View Playlist\n";
	cout<<"\n\nEnter the assigned number of your choice:"<<endl;
	cin>>num;
	if (num=="1"){
		system("CLS");
		string mus;
		cout<<"Music Lists:\n"<<endl;
		Music.DisplayPlaylist();
		cout<<"\n\n Input the title of music of your choice : "<<endl;
		cin.ignore();
		getline(cin,mus);
		Music.InsertNode(0,mus);
		goto again;
	}
	else if(num=="2"){
		int musnum;
		Music.DisplayPlaylist();
		cout<<"\n\nSelect the number assigned of the music to play "<<endl;
		cin>>musnum;
		if (musnum == 1){
			cout<<"Now Playing : "<<Music.ViewMusic(musnum)<<endl;
			cout<<"Next : "<<Music.ViewMusic(musnum+1)<<endl;
		goto again;
		}
		else{
			cout<<"Playing : "<<Music.ViewMusic(musnum)<<endl;
			cout<<"Up Next : "<<Music.ViewMusic(musnum+1)<<endl;
			cout<<"Previous : "<<Music.ViewMusic(musnum-1)<<endl;
			goto again;	
		}
		}
	else if(num=="3"){
	int editnum;
	string editname;
	Music.DisplayPlaylist();
	cout<<"\n\nInput and select song title to edit using the assigned number: "<<endl;
	cin>>editnum;
	Music.DelNode(editnum);
	cout<<"ENTER THE NEW MUSIC TITLE: ";
	cin.ignore();
	getline(cin,editname);
	cout<<endl;
	Music.InsertNode(editnum-1,editname);
	goto again;
	}
	else if(num=="4"){
	int del;
	Music.DisplayPlaylist();
	cout<<"\n\nInput Song Title to delete "<<endl;
	cout<<"*Disclaimer: Input only their assigned number beyond that error will occur"<<endl;
	cin>>del;
	Music.DelNode(del);
	goto again;
	}
	else if(num=="5"){
		Music.DisplayPlaylist();
		cout<<endl;
		goto again;
}
}